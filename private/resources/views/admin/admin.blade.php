@extends('app')

@section('content')
    <body id="dashboard" class="grey lighten-4">
    @include('admin.header')
     @yield('container')
    @include('admin.footer')
    </body>
@endsection