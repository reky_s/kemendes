@extends('admin.admin')
@section('menu-1','active')
@section('container')
    <div class="row">
        <div class="col s12 m12 l12" style="padding: 10px">
            <section id="content">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card">
                            @include('errors.error_notif')
                            <div class="card-content">
                                <span class="card-title black-text">Create Data</span>
                            </div>
                            <form class="col s12" action="{{URL::to('/user')}}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="first_name" type="text" class="validate" name="nama">
                                        <label for="first_name">Nama</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="last_name" type="text" class="validate" name="alamat">
                                        <label for="last_name">Alamat</label>
                                    </div>
                                </div>
                            <div class="card-action">
                                <button type="submit" class="waves-effect btn blue darken-2">Simpan</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
