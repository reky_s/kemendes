@extends('admin.admin')
@section('menu-2','active')
@section('container')
    <div class="row">
        <div class="col s12 m12 l12" style="padding: 10px">
            <section id="content">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card" style="padding:10px">
                            <div class="card-content">
                                @include('errors.error_notif')
                                <span class="card-title black-text">Create Data</span>
                            </div>
                            <table class="striped">
                                <thead>
                                <tr>
                                    <th data-field="id">No</th>
                                    <th data-field="id">Id User</th>
                                    <th data-field="id">Nama</th>
                                    <th data-field="name">Alamat</th>
                                    <th data-field="price">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $a=1;
                                foreach ($user as $u) {
                                ?>
                                <tr>
                                    <td>{{$a}}</td>
                                    <td>{{$u->id_user}}</td>
                                    <td>{{$u->nama}}</td>
                                    <td>{{$u->alamat}}</td>
                                    <td>
                                        <div class="input-field col s12 right">
                                            {!! Form::open(['url'=>'/user/'.$u->id_user]) !!}
                                            {!! Form::hidden('_method', 'DELETE') !!}
                                            <a href="{{URL::to('/user/'.$u->id_user.'/edit')}}" class="waves-effect btn green darken-2">Update</a>
                                            <button type="submit" class="waves-effect btn red darken-2" onclick="return confirm('Are you sure?')">Delete</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $a++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
