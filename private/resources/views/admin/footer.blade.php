<footer class="page-footer light-blue darken-2">
    <div class="footer-copyright">
        © 2014 Copyright <a href="http://djavabit.com" class="white-text">djavabit</a>
        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
    </div>
</footer>