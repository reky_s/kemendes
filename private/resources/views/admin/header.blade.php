<section id="header">
    <nav class="light-blue darken-2" style="padding: 0 10px 0 10px">
        <div class="nav-wrapper">
            <a href="#!" class="brand-logo">Logo</a>
            <ul class="right hide-on-med-and-down">
                <li class="@yield('menu-1')"><a href="{{URL::to('/user/create')}}">Create</a></li>
                <li class="@yield('menu-2')"><a href="{{URL::to('/')}}">Read</a></li>
            </ul>
        </div>
    </nav>
</section>