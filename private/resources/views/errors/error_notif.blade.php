@if(Session::has('message'))
    <div class="blue darken-2 center-align white-text">
        {{ Session::get('message') }}
    </div>
@endif