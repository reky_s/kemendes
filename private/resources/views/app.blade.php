<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>SILAU (Sistem Informasi Laundry)</title>
    {!! HTML::style('assets/plugins/materialize/css/materialize.min.css') !!}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
</head>
@yield('content')
{!! Html::script('assets/js/jquery-2.1.4.min.js') !!}
{!! Html::script('assets/plugins/materialize/js/materialize.js') !!}
<script type="text/javascript">
    var dashboard=function(){
        return{
            init:function(){
                dashboard.navbar();
            },
            navbar:function(){
                $(".button-collapse").sideNav();
                $(".dropdown-button").dropdown();
            }
        }
    }();
    dashboard.init();
</script>
</html>