<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Input;
use Redirect;
use Validator;
use Session;
use App\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $user =User::all();
        return view('admin.partial._read')->with(compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.partial._form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // Validation //
        $validation = Validator::make($request->all(), [
            'nama'  => 'required',
            'alamat' => 'required',
        ]);
//
//        // Check if it fails //
        if( $validation->fails() ){
            Session::flash('message', 'Data Gagal disimpan!');
            return redirect()->back()->withInput();
        }

        // Process valid data & go to success page //

        $user = new User;
        $user->nama= $request->input('nama');
        $user->alamat = $request->input('alamat');
        $user->save();
        Session::flash('message', 'Data Berhasil disimpan!');
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user =User::Where('id_user',$id)->get();
        return view('admin.partial._update')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        // Validation //
        $validation = Validator::make($request->all(), [
            'nama'  => 'required',
            'alamat' => 'required',
        ]);
//
//        // Check if it fails //
        if( $validation->fails() ){
            Session::flash('message', 'Data Gagal disimpan!');
            return redirect()->back()->withInput();
        }

        // Process valid data & go to success page //

        $user = User::find($id);
        $user->nama= $request->input('nama');
        $user->alamat = $request->input('alamat');
        $user->save();
        Session::flash('message', 'Data Berhasil disimpan!');
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        $user->delete();
        Session::flash('message', 'Data Berhasil dihapus!');
        return redirect('/user');
    }
}
